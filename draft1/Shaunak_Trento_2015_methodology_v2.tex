
\documentclass[a4paper,9pt, twocolumn]{article}

\usepackage[margin=0.5in]{geometry}
\usepackage{graphicx}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{bm}
\usepackage{tabularx}
\usepackage{subcaption}

\begin{document}
\section{Methodology}

\subsection{POLSAR DATA}

Measurement by a fully polarimetric SAR system commonly involves the transmission of horizontally (H) and vertically (V) polarized radar pulses followed by their coherent reception. This scattering matrix is said to be in the (H,V) polarization basis for and has the form:

\begin{equation}
\bm{S} =
  \begin{bmatrix}
    S_{hh} & S_{hv}  \\
    S_{vh} & S_{vv}
  \end{bmatrix}
 \label{eqn:scattring}
\end{equation}

The full polarimetric scattering matrix consisting of four independent measurements (HH, HV, VH and VV), with the phase relations between them preserved, at every imaged pixel~\cite{tragl1990polarimetric}. If we make an assumption that the targets are reciprocal, then $S_{HV} = S_{VH}$ and there is symmetry in the scattering matrix. The three unique elements can be used to define a vector of the form: $
\bm{u} ^\intercal = 
\begin{bmatrix}
S_{hh} & \sqrt{2}S_{hv} & S_{vv} \\
\end{bmatrix}
$. This vector, $\bm{u}$ has complex number elements. The term $S_{hv}$ is weighed by $\sqrt{2}$ to satisfy the condition that $|\bm{u}|^2$ = span(total power). 

The polarization state of an incident electromagnetic wave, in general, is altered on scattering from a complex radar target. This alteration is a function of the physical and geometrical properties of the target itself, which in turn can serve to characterize it~\cite{Boerner1981}. 

In most radar applications the target is susceptible to some temporal variation. If these variations occur over a time period larger then the illumination time of the radar, the target will appear to be relatively stable. Such targets are said to be deterministic or coherent. The scattering characteristics in this case can uniquely represented by the polarimetric scattering matrix ($\bm{S}$), which completely contains the information about the target at that radar frequency and scattering geometry. 

However, if the period of the temporal variation is comparable to the observation time of the radar, the target is said to be incoherent or random. This is especially true of natural targets which can be thought of as a volume of a large number of particles moving in a random manner, and are therefore time dependent. Thus when illuminated by a monochromatic radar, the descriptors of the scattered wave, like the amplitudes $a_h(t)$, $a_v(t)$ and phases $\delta_h(t)$, $\delta_v(t)$ are also time dependent. The target thus can be considered to generate a quasi-monochromatic field variations of the scattered waves.  In this case, the target must be characterized by a time average of functions with integration time larger than typical periods of the target's fluctuations~\cite{cox1978ellipsometry}. 

The Stokes vector representation or the covariance matrix~\cite{cox1978ellipsometry} can be used to describe the statistics of quasi-monochromatic waves. The Stokes formalism can be used to describe the target using the Mueller matrix, which can be related to the covariance matrix by linear transformations~\cite{cloude2009polarisation}.

In the case of symmetric mono-static scattering, we can constrain the Sinclair matrix, i.e. $S_{hv} = S_{vh}$, forming 3-D polarimetic target vectors. The Complex Pauli spin matrix basis set ${\{\Psi_P\}}$ is given by
 
 
\begin{equation}
{\Psi_P = \{ \sqrt{2}   
\begin{bmatrix}
    1 & 0  \\
    0 & 1
  \end{bmatrix} 
  \sqrt{2}   
  \begin{bmatrix}
      1 & 0  \\
      0 & -1
    \end{bmatrix} 
    \sqrt{2}   
    \begin{bmatrix}
        0 & 1  \\
        1 & 0
      \end{bmatrix} 
  \}}
  \label{eqn:pauli}
\end{equation}

The factors $2$, $\sqrt{2}$ or $2\sqrt{2}$ are added in equation~\ref{eqn:pauli} to account for total power invariance with


\begin{equation}
Span(S) = |\bm{k}|^2 = |S_{hh}|^2 + 2|S_{hv}|^2 + |S_{vv}|^2
\end{equation}


The corresponding $\bm{k}$-target vector is expressed as

\begin{equation}
\bm{k} = \frac{1}{\sqrt{2}} \begin{bmatrix}  S_{hh}+S_{vv} && S_{hh}-S_{vv} && 2S_{hv} \end{bmatrix} ^\intercal
\end{equation}



In the mono-static backscattering case, we can can construct a $3\times3$ polarimetric Pauli Coherency matrix, $\mathbf{[T_{3}]}$, as

\begin{equation}
	\mathbf{[T_{3}]} = \left< \bm{k} . \bm{k}^{*\intercal} \right>
\end{equation}

\begin{equation}
\mathbf{[T_{3}]} = \left< \begin{bmatrix}
t_1 & t_2 & t_3 \\
t_2^* & t_4 & t_5 \\
t_3^* & t_5^* & t_6 \\
\end{bmatrix} \right>
\end{equation}
on expansions we have, 
\begin{align*}
t_1 &= \langle |k_1|^2 \rangle  = \frac{1}{2} \langle|S_{hh}+S_{vv}|^2\rangle \\
t_2 &= \langle k_1k_2^* \rangle = \frac{1}{2} \langle|(S_{hh}+S_{vv})(S_{hh}-S_{vv})^*|^2\rangle \\
t_3 &= \langle k_1k_3^* \rangle = \langle (S_{hh}+S_{vv}) S_{hv}^* \rangle \\
%t4 &= \langle k_2k_1^* \rangle = \frac{1}{2} \langle|(S_{hh}-S_{vv})(S_{hh}+S_{vv})^*|^2\rangle \\
t_4 &= \langle |k_2|^2 \rangle  = \frac{1}{2} \langle|S_{hh}-S_{vv}|^2\rangle\\
t_5 &= \langle k_2k_3^*  \rangle = \langle (S_{hh}-S_{vv}) S_{hv}^* \rangle\\
%t7 &= \langle k_3k_1^*  \rangle = \langle S_{hv}(S_{hh}+S_{vv})^* \rangle\\
%t8 &= \langle k_3k_2^*  \rangle = \langle S_{hv}(S_{hh}-S_{vv})^* \rangle\\
t_6 &= \langle k_3k_2^*  \rangle = 2\langle |S_{hv}|^2 \rangle
\end{align*}



Where $\langle ... \rangle$ denote temporal or spatial ensemble averaging, under the assumption that the signal is ergodic~\cite{wyner1989some}. The $3\times3$ polarimetric coherency $\mathbf{[T_{3}]}$ matrix is a Hermition positive semidefinite matrix. It can the transformed to the $3\times3$ polarietric covarience matrix $\bm{C_3}$ by linear unitary transformations~\cite{lee2009polarimetric}.


\subsection{ROTATION}


\subsection{ROTATION NOVELTY}

\subsection{}

\bibliographystyle{unsrt}
%\bibliography{References/compact_classification_sar.bib,References/deepLearning.bib,References/polsar.bib,References/IGRASS2015.bib,References/ML.bib,References/polsar_stats.bib,References/remoteSensing.bib,References/SARobjectrecognition.bib,References/ml_books.bib,References/bigdata.bib}
\bibliography{References/compact_classification_sar,References/deepLearning,References/polsar,References/IGRASS2015,References/ML,References/polsar_stats,References/remoteSensing,References/SARobjectrecognition,References/ml_books,References/bigdata,References/urban,References/uavsar}
\end{document}